extends Node

var player = preload("res://player.tscn")
var tomb_popup = preload("res://tomb_popup.tscn")
var dig_popup = preload("res://tomb_dig.tscn")
var fill_popup = preload("res://tomb_fill.tscn")
var p = player.instance()

var del = 0
var stage

func _ready():
	set_process(true)
	
	global.current_scene = "stage3"
	
	### player creation
	if global.current_path == "2.2" :
		add_child(p)
		p.global_position = Vector2(2880, 660)
	if global.current_path == "4.1" :
		add_child(p)
		p.global_position = Vector2(1620, 660)
	if global.current_path == "4.3" :
		add_child(p)
		p.global_position = Vector2(5100, 660)
	if global.current_path == "to_3" :
		add_child(p)
		p.global_position = Vector2(600, 660)
	if global.current_path == "to_33" :
		add_child(p)
		p.global_position = Vector2(5550, 660)
	
func _enter_tree():
	global.current_stage = 3
	stage = 3
	global.tomb_small_num = 0
	global.tomb_medium_num = 0
	global.tomb_large_num = 0
	
func _process(delta):
	del -= delta # delay machine
	
	### paths actions
	if global.current_path == "3.1" :
		if Input.is_action_pressed("ui_up") && global.path_taken == false && Input.is_action_pressed("ui_left") == false && Input.is_action_pressed("ui_right") == false  :
				global.path_taken == true
				get_parent()._stage4_scene(stage)
				
	if global.current_path == "3.2" : # going forward
		if Input.is_action_pressed("ui_up") && global.path_taken == false && Input.is_action_pressed("ui_left") == false && Input.is_action_pressed("ui_right") == false  :
				global.path_taken == true
				get_parent()._stage2_scene(stage)
				
	if global.current_path == "3.3" : 
		if Input.is_action_pressed("ui_up") && global.path_taken == false && Input.is_action_pressed("ui_left") == false && Input.is_action_pressed("ui_right") == false  :
				global.path_taken == true
				get_parent()._stage4_scene(stage)
				
	### tombs actions
	if global.current_tomb != null && del <= 0 :
		if Input.is_action_pressed("ui_up"):
			del = 1
			var tomb_pop = tomb_popup.instance()
			add_child(tomb_pop)
	if global.current_tomb != null && del <= 0 :
		if Input.is_action_pressed("ui_down") && global.current_tomb_status != 0 && global.player_carying_corpse == false :
			del = 1
			var tomb_dig = dig_popup.instance()
			add_child(tomb_dig)
			get_tree().paused = true
	if Input.is_action_pressed("ui_down") && global.current_tomb_status == 0 && global.player_carying_corpse == true :
			del = 1
			var tomb_fill = fill_popup.instance()
			add_child(tomb_fill)
			get_tree().paused = true
			
#	if global.day_counter_end == true :
#		queue_free()