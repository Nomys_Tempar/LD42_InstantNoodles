extends Panel

var counter = preload("res://digging_counter.tscn")

var location

func _ready():
	get_node("yes").connect("pressed",self, "_yes_pressed")
	get_node("no").connect("pressed",self, "_no_pressed")
	
	location = get_parent().get_node("./player/camera").get_camera_screen_center()
	get_node(".").rect_position = Vector2(location)
	
	get_node("yes").grab_focus()

func _yes_pressed():
	
	### Small tombs
	if global.current_tomb_type == "small_tombs": 
		### Loading of the small
		var small = File.new()
		small.open("res://small_tombs.json", File.READ)
		var current_line = parse_json(small.get_as_text())
	
		### We delete the old value from the dict
		current_line[global.current_tomb].erase("state") 
		### Adding the new keys and values to the dict
		current_line[global.current_tomb]["state"] = 1

		### Writing new datas
		small.open("res://small_tombs.json", File.WRITE)
		small.store_line(to_json(current_line))
		small.close()
		
		global.small_empty.erase(global.current_tomb)
		
	### Medium tombs
	if global.current_tomb_type == "medium_tombs": 
		### Loading of the medium
		var medium = File.new()
		medium.open("res://medium_tombs.json", File.READ)
		var current_line = parse_json(medium.get_as_text())
	
		### We delete the old value from the dict
		current_line[global.current_tomb].erase("state") 
		### Adding the new keys and values to the dict
		current_line[global.current_tomb]["state"] = 1

		### Writing new datas
		medium.open("res://medium_tombs.json", File.WRITE)
		medium.store_line(to_json(current_line))
		medium.close()
		
		global.medium_empty.erase(global.current_tomb)
	
	### Large tombs
	if global.current_tomb_type == "large_tombs": 
		### Loading of the large
		var large = File.new()
		large.open("res://large_tombs.json", File.READ)
		var current_line = parse_json(large.get_as_text())
	
		### We delete the old value from the dict
		current_line[global.current_tomb].erase("state") 
		### Adding the new keys and values to the dict
		current_line[global.current_tomb]["state"] = 1

		### Writing new datas
		large.open("res://large_tombs.json", File.WRITE)
		large.store_line(to_json(current_line))
		large.close()
		
		global.large_empty.erase(global.current_tomb)
	
	### counter to dig
	var c = counter.instance()
	add_child(c)
	
	global.player_carying_corpse = false
	
	get_node("tomb_question").queue_free()
	get_node("yes").queue_free()
	get_node("no").queue_free()
	
	
func _end_digging():
	
	get_node("../tombs").get_child(global.current_tomb_index)._change_status()
	
	get_tree().paused = false
	queue_free()
	
func _no_pressed():
	queue_free()
	get_tree().paused = false