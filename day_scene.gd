extends Node

var day_counter = preload("res://day_counter.tscn")
var home_scene = preload("res://home_scene.tscn")
var stage1_scene = preload("res://stage1.tscn")
var stage2_scene = preload("res://stage2.tscn")
var stage3_scene = preload("res://stage3.tscn")
var stage4_scene = preload("res://stage4.tscn")
var stage5_scene = preload("res://stage5.tscn")
var black_counter

func _ready():
	set_process(true)
	
	black_counter = 3

	global.day_counter_on = false
	global.day_counter_end = false
	global.first_scene = false

	get_node("background").visible = true
	get_node("day_name").visible = true
	
func _process(delta):
	
	get_node("day_name").set_text("Day "+str(global.current_day))
	
	black_counter -= delta
	if black_counter < 0.1 && black_counter > 0:
		get_node("background").visible = false
		get_node("day_name").visible = false
		
		if global.day_counter_on == false :
			_home_scene()
			var dc = day_counter.instance()
			add_child(dc)
		
	if black_counter <= 0 :
		pass

### ending of the day - verifying missions
	if global.day_counter_end == true :
	
		if global.current_scene == "home":
			get_node("home_scene").queue_free()
		if global.current_scene == "stage1":
			get_node("stage1").queue_free()
		if global.current_scene == "stage2":
			get_node("stage2").queue_free()
		if global.current_scene == "stage3":
			get_node("stage3").queue_free()
		if global.current_scene == "stage4":
			get_node("stage4").queue_free()
		if global.current_scene == "stage5":
			get_node("stage5").queue_free()
	
		var large_ok
		var medium_ok
		var small_ok
	
		## we verify if the day missions are completed
		# large tombs
		var large_count = global.large_empty.size()

		if global.day_missions[0] <= large_count or (global.day_missions[0] == 0 && large_count < 0) :
			large_ok = true
		else :
			large_ok = false
	
		# medium tombs
		var medium_count = global.medium_empty.size()
	
		if global.day_missions[1] <= medium_count or (global.day_missions[1] == 0 && medium_count < 0) :
			medium_ok = true
		else :
			medium_ok = false
	
		# small tomb
		var small_count = global.small_empty.size()
	
		if global.day_missions[2] <= small_count or (global.day_missions[2] == 0 && small_count < 0) :
			small_ok = true
		else :
			small_ok = false
		
		### if the day's goals are fullfilled.
		if large_ok == true && medium_ok == true && small_ok == true :
			if global.current_day == 5 :
				get_tree().change_scene("res://game_over.tscn") # game over (success)
			else:
				global.current_day += 1# incrementing day counter
				
				var saved_largetomb_status = File.new() # large tombs
				saved_largetomb_status.open("res://large_tombs.json", File.READ)
				var large_status_current_line = parse_json(saved_largetomb_status.get_as_text())
				saved_largetomb_status.close()
				var index = 0
				for i in global.large_empty.size() : 
					large_status_current_line[global.large_empty[index]].erase("state")
					large_status_current_line[global.large_empty[index]]["state"] = 1
					index += 1
					
				### Writing new datas
				saved_largetomb_status.open("res://large_tombs.json", File.WRITE)
				saved_largetomb_status.store_line(to_json(large_status_current_line))
				saved_largetomb_status.close()
				
				global.large_empty.clear()
				
				var saved_mediumtomb_status = File.new() # medium tombs
				saved_mediumtomb_status.open("res://medium_tombs.json", File.READ)
				var medium_status_current_line = parse_json(saved_mediumtomb_status.get_as_text())
				saved_mediumtomb_status.close()
				index = 0
				for i in global.medium_empty.size() : 
					medium_status_current_line[global.medium_empty[index]].erase("state")
					medium_status_current_line[global.medium_empty[index]]["state"] = 1
					index += 1
					
				### Writing new datas
				saved_mediumtomb_status.open("res://medium_tombs.json", File.WRITE)
				saved_mediumtomb_status.store_line(to_json(medium_status_current_line))
				saved_mediumtomb_status.close()
				
				global.medium_empty.clear()
				
				var saved_smalltomb_status = File.new() # small tombs
				saved_smalltomb_status.open("res://small_tombs.json", File.READ)
				var small_status_current_line = parse_json(saved_smalltomb_status.get_as_text())
				saved_smalltomb_status.close()
				index = 0
				for i in global.small_empty.size() : 
					small_status_current_line[global.small_empty[index]].erase("state")
					small_status_current_line[global.small_empty[index]]["state"] = 1
					index += 1
					
				### Writing new datas
				saved_smalltomb_status.open("res://small_tombs.json", File.WRITE)
				saved_smalltomb_status.store_line(to_json(small_status_current_line))
				saved_smalltomb_status.close()
				
				global.small_empty.clear()
				_ready() # calling ready function again to start a new day
		else :
			get_tree().change_scene("res://game_over.tscn") # game over (failed)
		
### scenes manager
func _home_scene():
	if global.first_scene == true :
		get_node("stage1").queue_free()
	var hs = home_scene.instance()
	add_child(hs)
	
func _stage1_scene(stage):
	if stage == 0 :
		global.first_scene = true
		get_node("home_scene").queue_free()
	if stage == 2 :
		get_node("stage2").queue_free()
	if stage == 3 :
		get_node("stage3").queue_free()
	var s1s = stage1_scene.instance()
	add_child(s1s)

func _stage2_scene(stage):
	if stage == 1 :
		get_node("stage1").queue_free()
	if stage == 3 : 
		get_node("stage3").queue_free()
	if stage == 4 : 
		get_node("stage4").queue_free()
	var s2s = stage2_scene.instance()
	add_child(s2s)
	
func _stage3_scene(stage):
	if stage == 1 :
		get_node("stage1").queue_free()
	if stage == 2 : 
		get_node("stage2").queue_free()
	if stage == 4 : 
		get_node("stage4").queue_free()
	if stage == 5 : 
		get_node("stage5").queue_free()
	var s3s = stage3_scene.instance()
	add_child(s3s)
	
func _stage4_scene(stage):
	if stage == 2 :
		get_node("stage2").queue_free()
	if stage == 3 : 
		get_node("stage3").queue_free()
	if stage == 5 : 
		get_node("stage5").queue_free()
	var s4s = stage4_scene.instance()
	add_child(s4s)
	
func _stage5_scene(stage):
	if stage == 3 :
		get_node("stage3").queue_free()
	if stage == 4 : 
		get_node("stage4").queue_free()
	var s5s = stage5_scene.instance()
	add_child(s5s)