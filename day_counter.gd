extends Node

var counter = 300

func _ready():
	set_process(true)
	
	global.day_counter_on = true
	
	get_node("dc").rect_size = Vector2(100, 30)
	get_node("day_ends_in").rect_size = Vector2(100, 30)
	
	get_node("day_ends_in").set_text("Day Will End In...")
	
func _process(delta):
	
	counter -= delta
	get_node("dc").set_text(str(int(counter)))
		
	if global.current_scene == "home" :
		get_node("dc").visible = true
		get_node("day_ends_in").visible = true
		get_node("dc").rect_position = Vector2(1800,950)
		get_node("day_ends_in").rect_position = Vector2(1700,950)

	elif global.current_scene == "stage1" :
		get_node("dc").visible = true
		get_node("day_ends_in").visible = true
		get_node("dc").rect_position = Vector2(735,175)
		get_node("day_ends_in").rect_position = Vector2(635,175)

	elif global.current_scene == "stage5":
		get_node("dc").visible = true
		get_node("day_ends_in").visible = true
		get_node("dc").rect_position = Vector2(4500,600)
		get_node("day_ends_in").rect_position = Vector2(4450,580)

	else :
		get_node("dc").visible = false
		get_node("day_ends_in").visible = false
	
	### day is ending
	if counter < 0 :
		global.day_counter_end = true 
		queue_free()