extends Area2D

func _ready():
	connect("body_entered", self, "_common_grave_body_entered")
	connect("body_exited", self, "_common_grave_exited")

func _common_grave_body_entered(body):
	global.communale_grave = true
#	$anim.play("taken")

func _common_grave_exited(body):
	global.communale_grave = false