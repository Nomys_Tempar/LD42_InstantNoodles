extends Area2D

var stage = 1

func _ready():
	connect("body_entered", self, "_on_path_body_entered")

func _on_path_body_entered(body):
	if Input.is_action_pressed("ui_right") :
		global.current_path = "to_3"
		get_parent().get_parent()._stage3_scene(stage)
