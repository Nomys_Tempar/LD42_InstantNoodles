extends Area2D

var stage = 4

func _ready():
	connect("body_entered", self, "_on_path_body_entered")

func _on_path_body_entered(body):
	if Input.is_action_pressed("ui_left") :
		global.current_path = "to_2"
		get_parent().get_parent()._stage2_scene(stage)