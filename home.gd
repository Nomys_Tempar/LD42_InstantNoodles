extends Area2D

func _ready():
	connect("body_entered", self, "_on_path_body_entered")
	connect("body_exited", self, "_on_path_body_exited")

func _on_path_body_entered(body):
	global.home = true
#	$anim.play("taken")

func _on_path_body_exited(body):
	global.home = false
