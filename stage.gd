extends Node

var player = preload("res://player.tscn")
var tomb_popup = preload("res://tomb_popup.tscn")
var dig_popup = preload("res://tomb_dig.tscn")
var p = player.instance()

var del = 0

func _ready():
	set_process(true)
	
	global.current_scene = "stage"
	
	### player creation
	add_child(p)
	p.global_position = Vector2(900, 500)
	
func _enter_tree():
	global.current_stage = 1
	global.path_num = 0
	global.tomb_small_num = 0
	global.tomb_medium_num = 0
	global.tomb_large_num = 0
	
func _process(delta):
	del -= delta # delay machine
	
	### paths actions
	if global.current_path == "1.1" :
		if Input.is_action_pressed("ui_up"):
				p.global_position = Vector2(50, 500)
				
	### tombs actions
	if global.current_tomb != null && del <= 0 :
		if Input.is_action_pressed("ui_up"):
			del = 1
			var tomb_pop = tomb_popup.instance()
			add_child(tomb_pop)
	if global.current_tomb != null && del <= 0 :
		if Input.is_action_pressed("ui_down") && global.current_tomb_status != 0 :
			del = 1
			var tomb_dig = dig_popup.instance()
			add_child(tomb_dig)
			get_tree().paused = true