extends Panel

var location

func _ready():
	set_process(true)
	
	location = get_parent().get_node("./player/camera").get_camera_screen_center()
	get_node(".").rect_position = Vector2(location)
	get_node(".").rect_size = Vector2(500, 170)
	
	### we extract text from the json
	var tomb_text = File.new() 
	tomb_text.open("res://"+global.current_tomb_type+".json", File.READ)
	var current_line = parse_json(tomb_text.get_as_text())
	tomb_text.close()
	
	get_node("./tomb_text").set_text(current_line[global.current_tomb].tomb_text)
	get_node("./tomb_text").rect_position = Vector2(20, 10)
	get_node("./tomb_text").rect_size = Vector2(450, 170)
	
	get_tree().paused = true
	
func _process(delta):
	if Input.is_action_pressed("ui_accept"):
		get_tree().paused = false
		queue_free()