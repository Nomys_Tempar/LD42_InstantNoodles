extends Node

func _ready():
	set_process(true)
	
func _process(delta):
	
	if Input.is_action_pressed("ui_accept"):
	
		#### RELOADING SAVED CONDITIONS FOR THE GAME
		### Loading of the small save
		var small_save = File.new()
		small_save.open("res://small_tombs_save.json", File.READ)
		var small_current_line_save = parse_json(small_save.get_as_text())
		### Writing new small datas
		small_save.open("res://small_tombs.json", File.WRITE)
		small_save.store_line(to_json(small_current_line_save))
		small_save.close()
		
		### Loading of the medium save
		var medium_save = File.new()
		medium_save.open("res://medium_tombs_save.json", File.READ)
		var medium_current_line_save = parse_json(medium_save.get_as_text())
		### Writing new medium datas
		medium_save.open("res://medium_tombs.json", File.WRITE)
		medium_save.store_line(to_json(medium_current_line_save))
		medium_save.close()
		
		### Loading of the large save
		var large_save = File.new()
		large_save.open("res://large_tombs_save.json", File.READ)
		var large_current_line_save = parse_json(large_save.get_as_text())
		### Writing new large datas
		large_save.open("res://large_tombs.json", File.WRITE)
		large_save.store_line(to_json(large_current_line_save))
		large_save.close()
		
		get_tree().change_scene("res://day_scene.tscn")
