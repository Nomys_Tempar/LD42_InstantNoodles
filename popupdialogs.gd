extends PopupDialog

func _ready():
	set_process(true)
	popup_centered(Vector2(620, 210))
	
	### we extract text from the json
	var tomb_text = File.new() 
	tomb_text.open("res://"+global.current_tomb_type+".json", File.READ)
	var current_line = parse_json(tomb_text.get_as_text())
	tomb_text.close()
	
	get_node("./tomb_text").set_text(current_line[global.current_tomb].tomb_text)
	get_node("./tomb_text").push_align(1)
	get_node("./tomb_text").rect_position = Vector2(60, 20)
	get_node("./tomb_text").rect_size = Vector2(500, 170)
	
func _process(delta):
	if Input.is_action_pressed("ui_accept"):
		queue_free()
