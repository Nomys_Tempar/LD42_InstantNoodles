extends Node

var stage = 0
var day_num
var current_line

func _ready():
	set_process(true)
	
	global.current_scene = "home"
	
	day_num = "day_"+str(global.current_day)
	
	get_node("ledger").visible = false
	get_node("fax_letter").visible = false
	get_node("arrow_right").visible = false
	get_node("fax_letter/orders").visible = false
	get_node("ledger/ledger_open").visible = false
	
	_get_orders()

func _process(delta):
	
	if Input.is_action_pressed("ui_left") :
		get_node("fax_letter").visible = true
		get_node("ledger").visible = false
		get_node("arrow_right").visible = false

	if Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down") :
		get_node("ledger").visible = true
		get_node("fax_letter").visible = false
		get_node("arrow_right").visible = false

	if Input.is_action_pressed("ui_right") :
		get_node("ledger").visible = false
		get_node("arrow_right").visible = true
		get_node("fax_letter").visible = false

	if get_node("fax_letter").visible == false :
		get_node("fax_letter/orders").visible = false
	
	if get_node("ledger").visible == false :
		get_node("ledger/ledger_open").visible = false

	### go out
	if get_node("arrow_right").visible == true && Input.is_action_pressed("ui_accept"):
		get_parent()._stage1_scene(stage)

	### display day order
	if get_node("fax_letter").visible == true && Input.is_action_pressed("ui_accept") :
		get_node("fax_letter/orders").visible = true

		get_node("fax_letter/orders/large_num").set_text(str(current_line[day_num].large))
		get_node("fax_letter/orders/medium_num").set_text(str(current_line[day_num].medium))
		get_node("fax_letter/orders/small_num").set_text(str(current_line[day_num].small))
	
	### display ledger
	if get_node("ledger").visible == true && Input.is_action_pressed("ui_accept") :
		get_node("ledger/ledger_open").visible = true
	
#	if global.day_counter_end == true :
#		queue_free()

func _get_orders():
	### we extract text from the json to access missions datas
	var orders = File.new() 
	orders.open("res://missions.json", File.READ)
	current_line = parse_json(orders.get_as_text())
	orders.close()
	# store day missions in an array
	global.day_missions.append(current_line[day_num].large)
	global.day_missions.append(current_line[day_num].medium)
	global.day_missions.append(current_line[day_num].small)
	