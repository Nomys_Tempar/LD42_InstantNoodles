extends RichTextLabel

var counter = 5

func _ready():
	set_process(true)
	
	$".".rect_position = Vector2(185,50)
	$".".rect_size = Vector2(27, 29)
	
func _process(delta):
	
	set_text(str(int(counter)))
	
	counter -= delta
	
	if counter < 0 :
		get_node("..")._end_digging()