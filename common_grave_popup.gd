extends Panel

var location

func _ready():
	get_node("yes").connect("pressed",self, "_yes_pressed")
	get_node("no").connect("pressed",self, "_no_pressed")
	
	location = get_parent().get_node("./player/camera").get_camera_screen_center()
	get_node(".").rect_position = Vector2(location)
	
	get_node("yes").grab_focus()
	
	if global.player_carying_corpse == false :
		get_node("yes").visible = false
		get_node("no").set_text("ok")
		get_node("no").grab_focus()
		get_node("cg_dial").set_text("You don't have anything to bury here !")
		
	if global.player_carying_corpse == true :
		get_node("yes").visible = true
		get_node("yes").grab_focus()
		get_node("no").set_text("no")
		get_node("cg_dial").set_text("Do you want to put this body into this.... ?")

func _yes_pressed():
	
	global.player_carying_corpse = false
	
	get_tree().paused = false
	queue_free()
	
func _no_pressed():
	queue_free()
	get_tree().paused = false