extends Area2D

var path_id

func _ready():
	connect("body_entered", self, "_on_path_body_entered")
	connect("body_exited", self, "_on_path_body_exited")
	
	global.path_num += 1
	
	path_id = str(global.current_stage)+"."+str(global.path_num)

func _on_path_body_entered(body):
	if Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right") :
		global.current_path = path_id
#	$anim.play("taken")

func _on_path_body_exited(body):
	global.current_path = null
