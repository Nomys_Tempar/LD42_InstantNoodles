extends Node


func _ready():
	
	get_node("back_menu").connect("pressed", self, "_on_pressed")
	get_node("back_menu").set_text("Back to the Menu")
	get_node("back_menu").grab_focus()
	
	if global.current_day == 5 :
		get_node("game_over_text").set_text("Well done, you manage to keep the rythm !")
	else:
		get_node("game_over_text").set_text("Sorry, you didn't manage to free enough graves in time...")
	
func _on_pressed():
	get_tree().change_scene("res://menu_scene.tscn")