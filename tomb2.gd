extends Area2D

var tomb_fresh = preload("res://sprite_fresh.tscn")
var tomb_old = preload("res://sprite_old.tscn")
var tomb_empty = preload("res://sprite_empty.tscn")
var tomb_id
var tomb_status # tomb_status = 0 for empty, 1 for fresh, 2 for old

func _ready():
	set_process(true)
	connect("body_entered", self, "_on_tomb_body_entered")
	connect("body_exited", self, "_on_tomb_body_exited")
	
	global.tomb_medium_num += 1
	
	tomb_id = "medium_"+str(global.current_stage)+"."+str(global.tomb_medium_num)
	
	### we extract text from the json
	var saved_tomb_status = File.new() 
	saved_tomb_status.open("res://medium_tombs.json", File.READ)
	var current_line = parse_json(saved_tomb_status.get_as_text())
	saved_tomb_status.close()

	tomb_status = current_line[tomb_id].state
	
	if tomb_status == 0 :
		var grave = tomb_empty.instance()
		add_child(grave)
		grave.position = Vector2(0, 375)
	if tomb_status == 1 :
		var grave = tomb_fresh.instance()
		add_child(grave)
		grave.position = Vector2(0, 345)
	if tomb_status == 2 :
		var grave = tomb_old.instance()
		add_child(grave)
		grave.position = Vector2(0, 340)

func _on_tomb_body_entered(body):
	global.current_tomb = tomb_id
	global.current_tomb_type = "medium_tombs"
	global.current_tomb_status = tomb_status
	global.current_tomb_index = get_node(".").get_index()

#	$anim.play("taken")

func _on_tomb_body_exited(body):
	global.current_tomb = null
	global.current_tomb_type = null
	
func _change_status(): # when dig out or full up
	### we extract data from the json
	var saved_tomb_status = File.new() 
	saved_tomb_status.open("res://medium_tombs.json", File.READ)
	var current_line = parse_json(saved_tomb_status.get_as_text())
	saved_tomb_status.close()

	tomb_status = current_line[global.current_tomb].state
	
	get_child(2).queue_free()
	
	if tomb_status == 0 :
		var grave = tomb_empty.instance()
		add_child(grave)
		grave.position = Vector2(0, 375)
	if tomb_status == 1 :
		var grave = tomb_fresh.instance()
		add_child(grave)
		grave.position = Vector2(0, 345)
	if tomb_status == 2 :
		var grave = tomb_old.instance()
		add_child(grave)
		grave.position = Vector2(0, 340)